#
# https://gist.github.com/colinfrei/4038664
# # [...]
#
# #set :http_auth_users, [["username", "password"]]"
# #set :http_auth_file, ".htaccess"
# #set :http_auth_password_file, ".htpasswd"
# #set :http_auth_name, 'Restricted Area'
#
# before "deploy:create_symlink", "deploy:http_auth"
# # [...]

namespace :deploy do

  desc <<-DESC
    Generates / updates .htaccess and .htpasswd files with the credentials passed in parameter
  	Inspired from: https://gist.github.com/805879
  DESC
  task :http_auth do

    # Build htpasswd
    http_auth_users = fetch(:http_auth_users) { abort "Please specify the htaccess users, set :http_auth_users, [[\"username\", \"password\"]]" }

    if http_auth_users.empty? then
      abort "No htaccess users found, set :http_auth_users, [[\"username\", \"password\"]]"
    end

    htpasswdContent = ""

    http_auth_users.each do |user|

      if user[2].nil? then
        if user[1].nil? then
          abort "Please specify password or password-hash"
        end

        require 'digest/md5'

        #md5 = Digest::MD5.new << "#{user[1]}"
        #user[2] = md5.to_s
        abort "Please specify password-hash"

      end
      htpasswdContent += user[0] + ":" + user[2] + "\n"
    end

    #puts "'#{htpasswdContent}'"

    webroot = fetch(:webroot_http_auth,fetch(:webroot_base, ''))

    htaccess_file = "#{File.join(current_path,  webroot + '/.htaccess')}"
    htpasswd_file = "#{File.join(current_path,  webroot + '/.htpasswd')}"

    # This appends to the end of the file, so don't run it multiple times!
    on roles(:web) do

      #execute "echo '#{htpasswdContent}' >> #{htpasswd_file}"
      contents = StringIO.new(htpasswdContent)
      # Upload does not support within()
      upload! contents, htpasswd_file

      if test "[ -f #{htaccess_file} ]"
        execute "mv \"#{htaccess_file}\" \"#{htaccess_file}.bak\""
      else
        execute "touch \"#{htaccess_file}.bak\""
      end

      # Add all to .htaccess
      execute "echo 'AuthType Basic' >> #{htaccess_file}"
      execute "echo 'AuthName \"#{fetch(:http_auth_name, 'Restricted Area')}\"' >> #{htaccess_file}"
      execute "echo 'AuthUserFile #{htpasswd_file}' >> #{htaccess_file}"
      execute "echo 'Require valid-user' >> #{htaccess_file}"

      execute "cat #{htaccess_file}.bak >> #{htaccess_file} && rm #{htaccess_file}.bak"

      execute "chmod 755 #{htaccess_file}"
      execute "chmod 755 #{htpasswd_file}"
    end

  end
end
