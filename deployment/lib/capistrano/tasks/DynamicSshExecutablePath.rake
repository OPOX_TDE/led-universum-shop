Rake::Task["deploy:check"].clear_actions

namespace :deploy do
    task check: :'git:wrapper'  do
        on release_roles :all do
            execute :mkdir, "-p", "#{fetch(:tmp_dir)}/#{fetch(:application)}/"
            upload! StringIO.new("#!/bin/sh -e\nexec /usr/bin/env ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no \"$@\"\n"),fetch(:git_wrapper_path)
            execute :chmod, "+x", fetch(:git_wrapper_path)
        end
    end
end