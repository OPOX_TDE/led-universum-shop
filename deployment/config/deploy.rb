set :application, 'led-universum-shop'
set :repo_url, 'https://bitbucket.org/OPOX_TDE/led-universum-shop.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, %w{htdocs/media htdocs/var}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 10

# composer
set :composer_install_flags, '--no-dev --no-interaction --optimize-autoloader -v --ignore-platform-reqs'

# http auth
set :http_auth_users, [["digitalwert", "digitalwert", "$apr1$tR2q8gTZ$XVKGLp12CfjGTw/x/zZF60"], ["digi", "digi", "$apr1$8LFIIxub$kai4zU69y5XI96n9mN7y/."]]
set :webroot_base, 'htdocs'

# Filepermissions
set :file_permissions_chmod_mode, "644"

namespace :deploy do

    desc 'Clear Cache'
    task :clear_cache do
        on roles :app do
            cacheDirectory = shared_path.join('htdocs/var/cache')
            execute :rm, '-Rf', cacheDirectory
        end
    end

    before :published, 'deploy:clear_cache'
    before :finished, 'deploy:set_permissions:chmod'
end
