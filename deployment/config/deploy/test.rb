# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

set :tmp_dir, '/home/www/p130110/tmp/led-universum-shop.test'
set :deploy_to, '/home/www/p130110/html/led-universum.test/deploy/'
set :branch, 'develop'

# ssh p130110@led-universum.de
server 'led-universum.de', user: 'p130110', roles: %w{app db web}

namespace :deploy do

    namespace :symlink do

        desc 'Symlink Config'
        task :linked_configs do
            on roles :app do
                target = release_path.join('htdocs/app/etc/local.xml')
                source = release_path.join('htdocs/app/etc/config/test.xml')
                unless test "[ -L #{target} ]"
                    if test "[ -f #{target} ]"
                        execute :rm, target
                    end
                    execute :ln, '-s', source, target
                end
            end
        end

        after :linked_files, 'deploy:symlink:linked_configs'
    end

    before :published, 'deploy:http_auth'
end