# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

set :tmp_dir, '/home/www/p130110/tmp/led-universum-shop.staging2'
set :deploy_to, '/home/www/p130110/html/led-universum.staging2/deploy/'
set :branch, 'master'

# ssh p130110@led-universum.de
server 'led-universum.de', user: 'p130110', roles: %w{app db web}

# composer
SSHKit.config.command_map[:composer] = "COMPOSER_HOME='/home/www/p130110/html/led-universum.staging/.composer/' /usr/local/bin/php_cli #{shared_path.join("composer.phar")}"

# http auth
set :url_base, 'http://staging2.led-universum.de/'
set :http_auth_users, [["led", "led", "$apr1$VZG06ILn$XVsynnxZbMOhZXKA0QGiM."]]
set :webroot_base, 'htdocs'

namespace :deploy do

    namespace :symlink do

        desc 'Symlink Config'
        task :linked_configs do
            on roles :app do
                target = release_path.join('htdocs/app/etc/local.xml')
                source = release_path.join('htdocs/app/etc/config/staging2.xml')
                unless test "[ -L #{target} ]"
                    if test "[ -f #{target} ]"
                        execute :rm, target
                    end
                    execute :ln, '-s', source, target
                end
            end
        end

        after :linked_files, 'deploy:symlink:linked_configs'
    end

    before :published, 'deploy:http_auth'
end