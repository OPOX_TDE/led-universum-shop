# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

set :tmp_dir, '/var/www/tmp/wheezy.local/'
set :deploy_to, '/var/www/wheezy.local/'
set :branch, 'jtl-plugin-test'

# ssh admin@wheezy.local
server 'wheezy.local', user: 'admin', roles: %w{app db web}

# composer
SSHKit.config.command_map[:composer] = "COMPOSER_HOME='/var/www/wheezy.local/' php5 composer.phar"

namespace :deploy do

    namespace :symlink do

        desc 'Symlink Config'
        task :linked_configs do
            on roles :app do
                target = release_path.join('htdocs/app/etc/local.xml')
                source = release_path.join('htdocs/app/etc/config/wheezy.xml')

                error_xml = release_path.join('htdocs/errors/local.xml')
                error_sample = release_path.join('htdocs/errors/local.xml.sample')

                vendor_directory = release_path.join('vendor/')
                unless test "[ -L #{target} ]"
                    if test "[ -f #{target} ]"
                        execute :rm, target
                    end
                    execute :ln, '-s', error_sample, error_xml
                    execute :ln, '-s', source, target
                end
            end
        end

        after :linked_files, 'deploy:symlink:linked_configs'
    end

    before :published, 'deploy:http_auth'
end