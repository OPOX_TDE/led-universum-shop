# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:

set :tmp_dir, '/home/www/p130110/tmp/led-universum-shop.production'
set :deploy_to, '/home/www/p130110/html/led-universum.production/deploy/'
set :branch, 'master'

# ssh p130110@led-universum.de
server 'led-universum.de', user: 'p130110', roles: %w{app db web}

# composer
SSHKit.config.command_map[:composer] = "COMPOSER_HOME='/home/www/p130110/html/led-universum.staging/.composer/' /usr/local/bin/php_cli #{shared_path.join("composer.phar")}"

# http auth
set :url_base, 'https://www.led-universum.de/'
set :webroot_base, 'htdocs'

namespace :deploy do

    namespace :symlink do

        desc 'Symlink Config'
        task :linked_configs do
            on roles :app do
                target = release_path.join('htdocs/app/etc/local.xml')
                source = release_path.join('htdocs/app/etc/config/production.xml')
                unless test "[ -L #{target} ]"
                    if test "[ -f #{target} ]"
                        execute :rm, target
                    end
                    execute :ln, '-s', source, target
                end
            end
        end

        after :linked_files, 'deploy:symlink:linked_configs'
    end

end