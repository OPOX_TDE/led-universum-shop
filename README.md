# led-universum shop

## Environment Dependencies

* Apache 2.x
* PHP 5.4.42-CGI
* MySQL 5.6.x

## Magento Instanz

* Version: [1.9.2.2](https://www.magentocommerce.com/download#download1752)
* Patch-Level: [10975](https://magento.com/security/patches/supee-10975)

## Projektsetup

### Repository Checkout 

    git clone --recursive [-b <branch name>] git@bitbucket.org:digitalwert/led-universum-shop.git [<Ziel-Verzeichnis>]
    cd <Ziel-Verzeichnis>    
    
### Vagrant VM

    vagrant up
    
### Installation der Dependencies (composer)

    php composer.phar install
    
Einige Extensions werden in Form lokaler zip-Archive via composer installiert, da sie entweder nicht ueber composer-
Repositories oder github zur Verfuegung stehen oder direkte Anpassungen am Quelltext vorgenommen werden mussten.

Diese befinden sich unter: data/extensions

#### git Repositories

Die Installation aus git Repositories ist im verwendeten Mittwald-Hosting nicht möglich.

## vagrant

IP: 192.168.33.26

Domain www.led-universum.vagrant
    
### Provisioning

Auf Wunsch kopiert das Provisioning ein Datenbank-Fixture und installiert Dependencies via Composer.

siehe dazu provisioning/lamp.yml
    
### Dependencies

* vagrant 1.8.x
* ansible 2.1.x

## deployment

Das deployment erfolgt mittels [capistrano](http://capistranorb.com/).

### Workflow

Das Deployment wird via lokal erstelltem Archiv ausgeführt.

Alle auf dem Remote-System benoetigten Dateien muessen demnach lokal existieren.

* lokale Erzeugung eines Tar Archivs
* scp Kopiervorgang zum Zielserver
* Tar Archiv auf Zielserver entpacken

### Installation

siehe Gemfile, Capfile

    bundle install

### stage test

    bundle exec 'cap test deploy'

### stage staging

    bundle exec 'cap staging deploy'

    bundle exec 'cap staging-launch deploy'

### stage production

    bundle exec 'cap production deploy'

### CSS
    
    SCSS Dateien kompilieren in 'htdocs/skin/frontend/leduniversum/default/scss'
    'compass compile'

### ToDo

* Magento Core aus dem Repository entfernen und via composer nachladen

## digitalwert Magento Extensions

Dokumentation zu digitalwert Magento Extensions finden sich je Extension im zugehoerigen "code" Verzeichnis.

## Magento "led-universum" Theme

Die Dokumentaion zum Theme befindet sich im Theme-Verzeichnis.
    
## Fixtures

siehe data/fixtures

* configuration - enthaelt Magento Konfigurationen (z.B. fuer Vagrant)
* htdocs/media - enthaelt Beispieldaten fuer Magento "media"
* htdocs/var - enthaelt Beispieldaten fuer Magento "var"
* sql - sql Dump zur Verwendung in einer lokalen Entwicklungsumgebung

## statische Dateien

siehe data/staticfiles

* maintenance - Shop Wartungsseite
* product-image-default.png - Shop Produkt Standardbild
* tablerates.csv - Versandkostentabelle (importierbar)