<?php
// Magento login information 

// $url = "http://led-universum.local/index.php/";
$url = "http://led-universum.de/";

$proxy = new SoapClient($url . 'api/v2_soap/?wsdl');
// $sessionId = $proxy->login('test', 'testtest');
$sessionId = $proxy->login('JTL-Export', 'KacEpbyash3');

$csv_handler = fopen ('led-export.csv','w');
$media_header = array(
    "media_url_1", 
    "media_url_2", 
    "media_url_3", 
    "media_url_4", 
    "media_url_5", 
    "media_url_6", 
    "media_url_7", 
    "media_url_8", 
    "media_url_9"
);

$product_header = array(
    "sku", 
    "name", 
    "description", 
    "short_description", 
    "weight", 
    "price", 
    "url_key", 
    "url_path", 
);

$categoriy_header = array("categories");

// selecting all available attributes for selection in products
$attribute_name_list = array();
$attributes = new stdclass();

$attribute_sets = $proxy->catalogProductAttributeSetList($sessionId);

var_dump($attribute_sets);

foreach($attribute_sets as $attributeSet){
    $attribute_list = $proxy->catalogProductListOfAdditionalAttributes($sessionId, 'simple', $attributeSet->set_id);
    foreach($attribute_list as $attribute){
        array_push($attribute_name_list, $attribute->code);
    }
}
$attributes->additional_attributes = $attribute_name_list;

// write header
fputcsv($csv_handler, array_merge($product_header, $media_header, $attribute_name_list, $categoriy_header));

$product_list = $proxy->catalogProductList($sessionId);

// iterating over each found product
foreach ($product_list as $product){	
    // create arrays for usage
    $product_category_ids = array();
    $media_result = array_fill(0, 9, NULL);
    $attribute_result = array_fill(0, sizeof($attribute_name_list), NULL);

    $product_detail = $proxy->catalogProductInfo($sessionId, $product->product_id, NULL, $attributes);
  
    if($product_detail->type_id == "simple"){
        // attaching category information
        if ($product->category_ids){
            $product_category_ids = $product->category_ids;
        } else {
            array_push($product_category_ids, " ");
        }
        
        // attachin aditional attribute information        
        $i = 0;
        foreach($product_detail->additional_attributes as $additional_attribute){
            if($i>=sizeof($attribute_name_list)){
                break;
            }else{
               $attribute_result[$i] = $additional_attribute->value;
            }
        }

        // attaching media information
        $media_list = $proxy->catalogProductAttributeMediaList($sessionId, $product->product_id);

        $i = 0;
        foreach($media_list as $media){
            // add the first 9 images to the media settings
            if($i>=10){
                break;			
            }else{			
                $media_result[$i] = $media->url;
                $i++;
            }
        }
    } else {
        var_dump($product_detail);
    }
    fputcsv($csv_handler, 
        array_merge(
            array(
                $product->sku, 
                $product->name, 
                $product_detail->description, 
                $product_detail->short_description, 
                $product_detail->weight, 
                $product_detail->price,  
                $product_detail->url_key, $url . $product_detail->url_path
            ), 
            $media_result,
            $attribute_result,
            array(join(';', $product_category_ids))
        )
    );
}

fclose ($csv_handler);
?>