<?php
require_once('app/Mage.php');
ini_set('display_errors', 1);
Mage::app('admin');
class CAT
{

    public function index()
    {
        $root_category_id = 2;
        $copy_category_id = 23;       
                        
        $this->copy_categories($copy_category_id, $root_category_id, 'Copied Category');
    }
    
    public function copy_categories($copy_category, $parent_category, $category_name){
        $categoryApi = new Mage_Catalog_Model_Category_Api();
        
        $copy_category_obj = Mage::getModel('catalog/category')->load($copy_category);

        /* creating new category to attatch tree */
        $new_category_id = $categoryApi->create(
            $parent_category,
            array(
                'name'=>$category_name,
                'is_active'=>1,
                'include_in_menu'=>2,
                'is_active'=>$copy_category_obj->getData('is_active'),
                'custom_design '=>$copy_category_obj->getData('custom_design'),
                'custom_design '=>$copy_category_obj->getData('custom_design'),
                'custom_apply_to_products'=>$copy_category_obj->getData('custom_apply_to_products'),
                'custom_design_from'=>$copy_category_obj->getData('custom_design_from'),
                'custom_design_to'=>$copy_category_obj->getData('custom_design_to'),
                'custom_layout_update'=>$copy_category_obj->getData('custom_layout_update'),
                'default_sort_by'=>$copy_category_obj->getData('default_sort_by'),
                'display_mode'=>$copy_category_obj->getData('display_mode'),
                'is_anchor'=>$copy_category_obj->getData('is_anchor'),
                'landing_page'=>$copy_category_obj->getData('landing_page'),
                'meta_keywords'=>$copy_category_obj->getData('meta_keywords'),
                'meta_title'=>$copy_category_obj->getData('meta_title'),
                'page_layout'=>$copy_category_obj->getData('page_layout'),
                'url_key'=>"copied-" . $copy_category_obj->getData('url_key'),
                'include_in_menu'=>$copy_category_obj->getData('include_in_menu'),
                'filter_price_range'=>$copy_category_obj->getData('filter_price_range'),
                'custom_use_parent_settings'=>$copy_category_obj->getData('custom_use_parent_settings'),
                'description'=>$copy_category_obj->getData('description'),
                'custom_design '=>$copy_category_obj->getData('custom_design'),
                'meta_description '=>$copy_category_obj->getData('meta_description'),
                'available_sort_by'=>'position',
                'default_sort_by'=>'position'
            )
        );
               
        /* $products = Mage::getModel('catalog/product')->getCollection()->addCategoryFilter($copy_category_obj)->load(); 
        
        if(!empty($products)){
            foreach ($products as $product) {
                echo($product->getId());
                $categoryApi->assignProduct($copy_category, $product->getId());
            }
        } */
        
        $sub_categories = Mage::getModel('catalog/category')->getCategories($copy_category);
               
        if(!empty($sub_categories)){
            foreach ($sub_categories as $category) {
                $copy_category_name = Mage::getModel('catalog/category')->load($category->getId())->getName();
                $this->copy_categories($category->getId(), $new_category_id, $copy_category_name);
            }
        }
    }
}

$obj = new CAT();
$obj->index();

?>