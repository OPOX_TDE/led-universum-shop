<?php
header('HTTP/1.1 503 Service Temporarily Unavailable');
header('Status: 503 Service Temporarily Unavailable');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de" lang="de">
<head>
  <title>Error 503: Service Unavailable</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="description" content="" />
  <meta name="keywords" content="" />
  <meta name="robots" content="*" />
  <meta name="viewport" content="initial-scale=1.0, width=device-width">
  <link rel="icon" href="./favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon" />
  <style>
    html, body {
      height: 100%;
      background-color: #F9F9F9;
      font-family: Verdana, Arial, sans-serif;
      font-weight: 200;
      line-height: 1.5;
      color: #6F6F6F;
      margin: 0;
    }

    h1 {
      margin: 0 0 .7em;
      color: #97BD21;
      font-size: 27px;
      line-height: 1.2;
    }

    a {
      color: #97BD21;
      text-decoration: none;
    }

    a:hover {
      text-decoration: underline;
    }

    .page {
      width: 900px;
      margin: 0 auto;

      position: relative;
      top: 50%;
      transform: translateY(-50%);
    }

    .header-container {
      margin: 0 0 20px 0;
    }

    @media only screen and (max-width: 900px) {
      .page {
        width: auto;
        margin: 0 10px;
      }
    }

    @media only screen and (max-width: 320px) {
      img {
        width: 100%;
      }
    }
  </style>
</head>
<body>
  <div class="page">
    <div class="header-container">
      <div class="header">
        <a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>">
          <img src="./logo.png" alt="LED-Universum" />
        </a>
      </div>
    </div>
    <div class="main-container">
      <div class="main col1-layout">
        <div id="main" class="col-main">
          <div class="page-title">
            <h1>Wartungs&shy;arbeiten</h1>
          </div>
          <p>
            Aufgrund einer Aktualisierung der Webseite ist unser Onlineshop bis ca. 17 Uhr nicht erreichbar.
            Bitte kontaktieren Sie uns in dieser Zeit per <a href="info@led-universum.de">E-Mail</a> oder Telefon (+49 (0) 351 65335641).
          </p>
          <p>
            Vielen Dank für Ihr Verständnis.<br />
            Freundliche Grüße<br />
            Ihr LED Universum Team.
          </p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>