# Theme leduniversum/default

* erbt von "rwd/default"

## Layout

Anpassungen am Layout wurden, soweit moeglich, ueber die local.xml vorgenommen.

## Uebersetzungen

Es wurden einige Theme-spezifische Uebersetzungen (DE) im Theme hinterlegt.

## Development

### SVG Icon Sprite

Die im Theme verwendeten Icons sind im SVG-Format hinterlegt. Zur Performanceoptimierung werden alle einzelnen
SVG Dateien in einem sog. "Sprite" zusammengefasst und die so entstehende SVG Datei entsprechend referenziert.

Die Erstellung dieser Sprite-Datei erfolgt mit Hilfe von [nodeJS](https://nodejs.org) und [Grunt](http://gruntjs.com/).

siehe htdocs/skin/frontend/leduniversum/dev/

#### Installation Dependencies

Eine systemweite Installation der [grunt-cli](https://github.com/gruntjs/grunt-cli) ist erforderlich:

    npm install -g grunt-cli

Anschliessend koennen die lokalen Dependencies installiert werden:

    npm install
    
#### Erstellung des SVG Icon Sprite

Als Quelle dient das Verzeichnis svgs/ wobei ein darin befindliches
Unterverzeichnis als Name des resultierenden Sprite fungiert.

Die Quelldateien koennen mit Hilfe eines separaten Grunt Tasks optimiert werden:

    grunt svgmin

Die Erstellung des SVG Sprite erfolgt mit Hilfe eines Grunt Tasks:
    
    grunt svgstore

Anschliessend befindet sich im Verzeichnis icons/ die Zieldatei.

### css Preprocessor

Die im Theme verwendeten CSS Dateien werden mit Hilfe des CSS-Preprocessor [sass](http://sass-lang.com/) und einer
darauf aufsetzenden Erweiterung namens [compass](http://compass-style.org/) entwickelt.

#### Installation Dependencies

Compass und dessen Dependencies koennen ueber das im Repository-Root befindliche Gemfile mit Hilfe von Bundler
installiert werden.

    bundle install
    
#### automatische Konvertierung von sass zu css

siehe htdocs/skin/frontend/leduniversum/default/scss/

Compass kann mit Hilfe des folgenden Befehls gestartet werden. Es ueberwacht die Quelldateien (scss) auf Veraenderungen
und fuehrt die css Konvertierung bei Bedarf aus.

    bundle exec "compass watch"

Das Zielverzeichnis ist in der Konfigurationsdatei "config.rb" hinterlegt, es lautet:

htdocs/skin/frontend/leduniversum/default/css/

## Hinweise

### Inhaltselemente

Ein Grossteil der Inhaltselemente im Contentbereich sind mit einem Wrapper ".tile-content" versehen.

Soll in einem Element frei pflegbares HTML dargestellt werden (z.B. CMS Page oder CMS Block), lautet dieser
Wrapper ".tile-content.text-content" und formatiert z.B. HTML Tabellen oder Listen speziell fuer diesen
Anwendungsbereich.

### zusaetzliche Layout-Handles

#### content_fullwidth

Es wurde ein zusaetzlicher Layout-Bereich "content_fullwidth" angelegt (siehe layout/local.xml::default::root).
Dieser wird ueber die volle Breite oberhalb des Content-Bereichs platziert.

Dieser wird auf Kategorieseiten (catalog_category_default) verwendet.

#### content_fullwidth_bottom

Es wurde ein zusaetzlicher Layout-Bereich "content_fullwidth_bottom" angelegt (siehe layout/local.xml::default::root).
Dieser wird ueber die volle Breite unterhalb des Content-Bereichs platziert.

Dieser wird auf der Startseite (cms_index_index) verwendet.

#### content_teasers

Es wurde ein zusaetzlicher Layout-Bereich "content_teasers" angelegt (siehe layout/local.xml::default::root).

Dieser wird fuer die Platzierung von Widgets auf der Startseite verwendet
(siehe layout/local.xml::cms_index_index::ms.wrapper.inner).

#### Teaser

Die Teaserspalte (left) wird via xml-Layout mit fest definierten Teaser-Elementen befuellt
(siehe layout/local.xml::default::left).

Es handelt sich bei diesen Elementen um CMS Bloecke, wobei der Block "Title" als Ueberschrift des Teasers
verwendet wird.

Templates siehe template/cms/teaser/ 