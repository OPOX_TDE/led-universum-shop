<?php

/**
 * @author Thorsten Blei
 * Date: 04.01.17
 */
class Digitalwert_Productflags_Block_Flags extends Mage_Core_Block_Template
{

    /**
     *
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return bool
     */
    public function isNewProduct(Mage_Catalog_Model_Product $product) {
        $newsFromDate = $product->getNewsFromDate();
        $newsToDate = $product->getNewsToDate();

        if (!$newsFromDate && !$newsToDate) {
            return false;
        }

        return Mage::app()->getLocale()->isStoreDateInInterval($product->getStoreId(), $newsFromDate, $newsToDate);
    }

    /**
     *
     *
     * @param Mage_Catalog_Model_Product $product
     *
     * @return bool
     */
    public function isInStockProduct(Mage_Catalog_Model_Product $product) {
        if ($product->getStockItem() && $product->getStockItem()->getIsInStock()) {
            return true;
        } else {
            return false;
        }
    }

}