<?php

class Digitalwert_Sitemap_Model_Resource_Catalog_Category extends Mage_Sitemap_Model_Resource_Catalog_Category
{

    /**
     * Get category collection array
     *
     * @param int $storeId
     * @return array
     */
    public function getCollection($storeId)
    {
        /* @var $store Mage_Core_Model_Store */
        $store = Mage::app()->getStore($storeId);
        if (!$store) {
            return false;
        }

        $this->_select = $this->_getWriteAdapter()->select()
            ->from($this->getMainTable())
            ->where($this->getIdFieldName() . '=?', $store->getRootCategoryId());

        $categoryRow = $this->_getWriteAdapter()->fetchRow($this->_select);
        if (!$categoryRow) {
            return false;
        }

        $this->_select = $this->_getWriteAdapter()->select()
            ->from(array('main_table' => $this->getMainTable()), array($this->getIdFieldName()))
            ->where('main_table.path LIKE ?', $categoryRow['path'] . '/%');

        $storeId = (int)$store->getId();

        /** @var $urlRewrite Mage_Catalog_Helper_Category_Url_Rewrite_Interface */
        $urlRewrite = $this->_factory->getCategoryUrlRewriteHelper();
        $urlRewrite->joinTableToSelect($this->_select, $storeId);

        $this->_addFilter($storeId, 'is_active', 1);

        $customLayoutUpdateAttributeCode = 'custom_layout_update';
        $this->_loadAttribute($customLayoutUpdateAttributeCode);
        $customLayoutUpdateAttribute = $this->_attributesCache[$customLayoutUpdateAttributeCode];

        //var_dump($customLayoutUpdateAttribute);

        $this->_select->join(
            array('t1_' . $customLayoutUpdateAttributeCode => $customLayoutUpdateAttribute['table']),
            'main_table.entity_id=t1_' . $customLayoutUpdateAttributeCode . '.entity_id AND t1_' . $customLayoutUpdateAttributeCode . '.store_id=0',
            array()
        );
        $this->_select->where('t1_' . $customLayoutUpdateAttributeCode . '.attribute_id=?', $customLayoutUpdateAttribute['attribute_id']);
        $this->_select->joinLeft(
            array('t2_' . $customLayoutUpdateAttributeCode => $customLayoutUpdateAttribute['table']),
            $this->_getWriteAdapter()->quoteInto(
                't1_' . $customLayoutUpdateAttributeCode . '.entity_id = t2_' . $customLayoutUpdateAttributeCode . '.entity_id AND t1_'
                . $customLayoutUpdateAttributeCode . '.attribute_id = t2_' . $customLayoutUpdateAttributeCode . '.attribute_id AND t2_'
                . $customLayoutUpdateAttributeCode . '.store_id = ?', $storeId
            ),
            array()
        );

        $this->_select->columns('value', 't1_custom_layout_update');

        return $this->_loadEntities();
    }

    /**
     * Prepare catalog object
     *
     * @param array $row
     * @return Varien_Object
     */
    protected function _prepareObject(array $row)
    {
        $entity = new Varien_Object();
        $entity->setId($row[$this->getIdFieldName()]);
        $entity->setUrl($this->_getEntityUrl($row, $entity));
        $entity->setCustomLayoutUpdate($row['value']);
        return $entity;
    }

    /**
     * Load and prepare entities
     *
     * @return array
     */
    protected function _loadEntities()
    {
        $entities = array();
        $query = $this->_getWriteAdapter()->query($this->_select);
        while ($row = $query->fetch()) {
            $entity = $this->_prepareObject($row);

            if((strpos($entity->getCustomLayoutUpdate(), 'noindex') != 0) || (strpos($entity->getCustomLayoutUpdate(), 'nofollow') != 0)) {
                continue;
            }

            $entities[$entity->getId()] = $entity;
        }
        return $entities;
    }
}