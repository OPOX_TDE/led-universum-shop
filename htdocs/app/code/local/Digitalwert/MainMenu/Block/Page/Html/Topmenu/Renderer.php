<?php

class Digitalwert_MainMenu_Block_Page_Html_Topmenu_Renderer extends Mage_Page_Block_Html_Topmenu_Renderer
{
    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = [];

        $classes[] = "mainmenu-item";

        return $classes;
    }

    protected function getStaticMenuFor(Varien_Data_Tree_Node $item) {
        /* @var $staticBlock Digitalwert_MainMenu_Block_Page_Html_Topmenu_StaticChild */
        $staticBlock = $this->getLayout()->createBlock('digitalwert_mainmenu/page_html_topmenu_staticchild');
        $staticBlock->setTemplate('page/html/topmenu/static.phtml');

        $staticBlock->setMenuItem($item);

        return $staticBlock->toHtml();
    }
}