<?php

class Digitalwert_MainMenu_Block_Page_Html_Mobilemenu_Renderer extends Mage_Page_Block_Html_Topmenu_Renderer
{
    /**
     * Returns array of menu item's classes
     *
     * @param Varien_Data_Tree_Node $item
     * @return array
     */
    protected function _getMenuItemClasses(Varien_Data_Tree_Node $item)
    {
        $classes = [];

        $classes[] = "mobile-menu_item";

        return $classes;
    }

}