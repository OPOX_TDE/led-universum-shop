<?php

class Digitalwert_MainMenu_Block_Page_Html_Topmenu_StaticChild extends Mage_Core_Block_Template
{
    /**
     * @var Varien_Data_Tree_Node
     */
    protected $menuItem;

    /**
     * @return Varien_Data_Tree_Node
     */
    public function getMenuItem() {
        return $this->menuItem;
    }

    /**
     * @param Varien_Data_Tree_Node $menuItem
     */
    public function setMenuItem(Varien_Data_Tree_Node $menuItem) {
        $this->menuItem = $menuItem;
    }

    /**
     * @return array|null
     */
    public function getCmsStaticBlock() {
        if (!$this->menuItem || !($this->menuItem instanceof Varien_Data_Tree_Node)) {
            return null;
        }

        $itemId = $this->menuItem->getId();

        if (empty($itemId)) {
            return null;
        }

        $staticBlocks = [];

        for ($i = 1; $i <= 7; $i++) {
            $blockIdentifier = $itemId . '-' . $i;
            $staticBlocks[] = Mage::getModel('cms/block')->load($blockIdentifier, 'identifier');
        }

        return $staticBlocks;
    }

    public function getBlockTemplateProcessor() {
        $helper = Mage::helper('cms');
        return $helper->getBlockTemplateProcessor();
    }

}