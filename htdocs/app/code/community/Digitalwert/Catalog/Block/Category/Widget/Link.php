<?php

class Digitalwert_Catalog_Block_Category_Widget_Link extends Mage_Catalog_Block_Category_Widget_Link
{

  /**
   * Prepare url using passed id and return it
   * or return false if path was not found.
   *
   * @return string|false
   */
  public function getHref() {
    if (!$this->_href) {
      $idPath = explode('/', $this->_getData('id_path'));

      if (isset($idPath[0]) && isset($idPath[1]) && $idPath[0] == 'product') {

        /** @var $helper Mage_Catalog_Helper_Product */
        $helper = $this->_getFactory()->getHelper('catalog/product');
        $productId = $idPath[1];
        $categoryId = isset($idPath[2]) ? $idPath[2] : null;

        $this->_href = $helper->getFullProductUrl($productId, $categoryId);

      } elseif (isset($idPath[0]) && isset($idPath[1]) && $idPath[0] == 'category') {
        $categoryId = $idPath[1];
        if ($categoryId) {
          /** @var $helper Mage_Catalog_Helper_Category */
          $helper = $this->_getFactory()->getHelper('catalog/category');
          $category = Mage::getModel('catalog/category')->load($categoryId);
          $this->_href = $helper->getCategoryUrl($category);
        }
      }
    }

    if (!$this->_href) {
      return false;
    }

    return $this->_href;
  }
}
