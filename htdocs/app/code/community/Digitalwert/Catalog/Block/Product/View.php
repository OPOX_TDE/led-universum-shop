<?php

class Digitalwert_Catalog_Block_Product_View extends Mage_Catalog_Block_Product_View
{

    /**
    * Add meta information from product to head block
    *
    * @return Mage_Catalog_Block_Product_View
    */
    protected function _prepareLayout()
    {
        $product = $this->getProduct();

        if ($product->getHersteller()) {
            $hersteller = $product->getHersteller() . ' ';
        } else {
            $hersteller = '';
        }

        $product->setMetaTitle(
            $hersteller . $product->getName()
        );

        /** @var Mage_Catalog_Helper_Data $catalogHelper */
        $catalogHelper = $this->helper('catalog');

        $product->setMetaDescription(
            $catalogHelper->__('META_DESCRIPTION_PREFIX') .
            $product->getName() .
            $catalogHelper->__('META_DESCRIPTION_SUFFIX')
        );

        $this->setProduct($product);

        return parent::_prepareLayout();
    }

}