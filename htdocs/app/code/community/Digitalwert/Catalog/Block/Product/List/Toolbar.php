<?php

class Digitalwert_Catalog_Block_Product_List_Toolbar extends Mage_Catalog_Block_Product_List_Toolbar
{

  protected $pagerShouldRenderRel = true;

  /**
   * @return boolean
   */
  public function getPagerShouldRenderRel () {
    return $this->pagerShouldRenderRel;
  }

  /**
   * @param boolean $pagerShouldRenderRel
   */
  public function setPagerShouldRenderRel ($pagerShouldRenderRel) {
    $this->pagerShouldRenderRel = $pagerShouldRenderRel;
  }

  /**
   * Render pagination HTML
   *
   * @return string
   */
  public function getPagerHtml () {
    $pagerBlock = $this->getChild('product_list_toolbar_pager');

    if ($pagerBlock instanceof Varien_Object) {

      /* @var $pagerBlock Mage_Page_Block_Html_Pager */
      $pagerBlock->setAvailableLimit($this->getAvailableLimit());

      $pagerBlock->setUseContainer(false)
          ->setShowPerPage(false)
          ->setShowAmounts(false)
          ->setLimitVarName($this->getLimitVarName())
          ->setPageVarName($this->getPageVarName())
          ->setLimit($this->getLimit())
          ->setFrameLength(Mage::getStoreConfig('design/pagination/pagination_frame'))
          ->setJump(Mage::getStoreConfig('design/pagination/pagination_frame_skip'))
          ->setCollection($this->getCollection())
          ->setShouldRenderRel($this->getPagerShouldRenderRel());

      return $pagerBlock->toHtml();
    }

    return '';
  }
}
