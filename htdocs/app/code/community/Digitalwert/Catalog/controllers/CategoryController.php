<?php
require_once(Mage::getModuleDir('controllers', 'Mage_Catalog') . DS . 'CategoryController.php');

/**
 * Category controller
 */
class Digitalwert_Catalog_CategoryController extends Mage_Catalog_CategoryController
{

  /**
   * Category view action
   */
  public function viewAction () {
    if ($category = $this->_initCatagory()) {
      $design = Mage::getSingleton('catalog/design');
      $settings = $design->getDesignSettings($category);

      // apply custom design
      if ($settings->getCustomDesign()) {
        $design->applyCustomDesign($settings->getCustomDesign());
      }

      Mage::getSingleton('catalog/session')->setLastViewedCategoryId($category->getId());

      $update = $this->getLayout()->getUpdate();
      $update->addHandle('default');

      if (!$category->hasChildren()) {
        $update->addHandle('catalog_category_layered_nochildren');
      }

      $this->addActionLayoutHandles();
      $update->addHandle($category->getLayoutUpdateHandle());
      $update->addHandle('CATEGORY_' . $category->getId());
      $this->loadLayoutUpdates();

      // apply custom layout update once layout is loaded
      if ($layoutUpdates = $settings->getLayoutUpdates()) {
        if (is_array($layoutUpdates)) {
          foreach ($layoutUpdates as $layoutUpdate) {
            $update->addUpdate($layoutUpdate);
          }
        }
      }

      $this->generateLayoutXml()->generateLayoutBlocks();
      // apply custom layout (page) template once the blocks are generated
      if ($settings->getPageLayout()) {
        $this->getLayout()->helper('page/layout')->applyTemplate($settings->getPageLayout());
      }

      if ($root = $this->getLayout()->getBlock('root')) {
        $root->addBodyClass('categorypath-' . $category->getUrlPath())
            ->addBodyClass('category-' . $category->getUrlKey());
      }

      $this->_initLayoutMessages('catalog/session');
      $this->_initLayoutMessages('checkout/session');

      //Robots
      $attributeInRequest = false;
      $layer = Mage::getModel("catalog/layer");
      $layer->setCurrentCategory($category);
      $filterAttributes = $layer->getFilterableAttributes();

      foreach ($filterAttributes as $_attribute) {
        $attributeInRequestValue = $this->getRequest()->getParam($_attribute->getAttributeCode(), null);
        if ($attributeInRequestValue != null) {
          $attributeInRequest = true;
          break;
        }
      }

      $page = $this->getRequest()->getParam('p', null);
      $limit = $this->getRequest()->getParam('limit', null);
      $orderDirection = $this->getRequest()->getParam('dir', null);
      $orderAttribute = $this->getRequest()->getParam('order', null);
      $mode = $this->getRequest()->getParam('mode', null);

      if ($attributeInRequest || $orderDirection != null || $orderAttribute != null || $limit != null || $mode != null || $page != null) {
        $headBlock = $this->getLayout()->getBlock('head');
        $headBlock->setRobots('NOINDEX, FOLLOW', 'robots');
      }

      $this->renderLayout();
    } elseif (!$this->getResponse()->isRedirect()) {
      $this->_forward('noRoute');
    }
  }

}