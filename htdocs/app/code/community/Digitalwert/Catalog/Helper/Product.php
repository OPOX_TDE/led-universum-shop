<?php

class Digitalwert_Catalog_Helper_Product extends Mage_Catalog_Helper_Product
{
    const IMAGE_SIZE_AVERAGE = 540;

    const IMAGE_SIZE_MEDIUM = 600;

    const IMAGE_SIZE_BIG = 1500;
}
