<?php

class Digitalwert_Catalog_Helper_Category extends Mage_Catalog_Helper_Category
{
  const CATEGORY_SERVICE_ID_XMLPATH = 'catalog/default/category_service_id';
  const CATEGORY_BRANDS_ID_XMLPATH = 'catalog/default/category_brands_id';

  /**
   * @param Mage_Catalog_Model_Category $category
   * @param int                         $parentCategoryId
   *
   * @return bool
   */
  public function isCategoryChildOf(Mage_Catalog_Model_Category $category, $parentCategoryId) {
    $path = $category->getPath();
    $path = explode('/', $path);

    array_shift($path); //magento root kategorie entfernen (id: 1)
    array_shift($path); //shop root kategorie entfernen (id: 2)

    $return = false;

    foreach ($path as $p) {
      if ($p == $parentCategoryId) {
        $return = true;
        break;
      }
    }

    return $return;
  }

  /**
   *
   *
   * @param Mage_Catalog_Model_Product $
   *
   * @return Mage_Catalog_Model_Category
   */
  public function getBrandCategory(Mage_Catalog_Model_Product $product) {
    $brandsRootCategoryId = Mage::getStoreConfig(self::CATEGORY_BRANDS_ID_XMLPATH);

    if (empty($brandsRootCategoryId)) {
      return null;
    }

    $brandsRootCategory = Mage::getModel('catalog/category')
        ->load($brandsRootCategoryId);

    $productBrandCategories = $product->getCategoryCollection()
        ->addAttributeToSelect('entity_id')
        ->addAttributeToSelect('name')
        ->addUrlRewriteToResult()
        ->addFieldToFilter('path', ['like' => $brandsRootCategory->getData('path') . '/%'])
        ->addLevelFilter($brandsRootCategory->getLevel() + 1)
        ->setPageSize(1);

    if ($productBrandCategories->count() == 0) {
      return null;
    }

    return $productBrandCategories->getFirstItem();
  }

}