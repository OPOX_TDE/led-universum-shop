# Extension Digitalwert_Catalog

## Zweck

* Erweiterung des Magento Core Moduls Mage_Catalog
* dynamische Anpassung des html head tags "meta robots"
* Ausgabe von "link rel" Tags fuer den Pager um URLs zur vorherigen und/oder naechsten Seite zu generiern

## Implementierung

**dynamische Anpassung des html head tags "meta robots"**

Controller "Category" erweitert

Sollte ein Magento Produktfilter aktiv sein (filterable attribute in URL als Parameter vorhanden) oder URL-Parameter
fuer die Sortierung (dir, order), die Ergebnisse pro Seite (limit) oder den Darstellungsmodus (mode) vorhanden sein,
sollte das meta robots Tag den Inhalt "NOINDEX, FOLLOW" aufweisen.

Ist hingegen der Pager Parameter (page) vorhanden und hat den Wert "1", wird keine Veraenderung des meta robots Tag
vorgenommen.

**Ausgabe von "link rel" Tags fuer den Pager**

Block Mage_Catalog_Block_Product_List_Toolbar erweitert

Es wurde ein Attribut sowie ein Getter und Setter zur Block-Klasse hinzugefuegt. Der Wert des Attributs wird an
den Pager-Block uebergeben und kann im Template ausgewertet werden (siehe page/html/pager.phtml).

**zusaetzliche Helper**

* Category
* Product

## Dependencies

* Mage_Catalog

## Konfiguration

keine