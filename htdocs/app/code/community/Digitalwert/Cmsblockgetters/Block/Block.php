<?php

/**
 * Cms block content block
 *
 * @category   Digitalwert
 * @package    Digitalwert_Cms
 * @author     Steve Rohrlack <steve.rohrlack@digitalwert.de>
 */
class Digitalwert_Cmsblockgetters_Block_Block extends Mage_Cms_Block_Block
{

  //const CACHE_LIFETIME = 86400;

  //const CACHE_KEY = 'DIGITALWERT_CMS_BLOCK_BLOCK';

  /**
   * Initialize block's cache
   */
  /*protected function _construct() {
    parent::_construct();

    $this->addData(array('cache_lifetime' => self::CACHE_LIFETIME));
    $this->addCacheTag(Mage_Cms_Model_Block::CACHE_TAG);
  }*/

  /**
   * Get Key pieces for caching block content
   *
   * @return array
   */
  /*public function getCacheKeyInfo() {
    return array(
      self::CACHE_KEY,
      Mage::app()->getStore()->getId(),
      Mage::getDesign()->getPackageName(),
      Mage::getDesign()->getTheme('template'),
      'template' => $this->getTemplate(),
    );
  }*/

  /**
   *
   * @return mixed
   */
  protected function _getAvailableBlock() {
    $blockId = $this->getBlockId();

    if ($blockId) {
      $block = Mage::getModel('cms/block')
        ->setStoreId(Mage::app()->getStore()->getId())
        ->load($blockId);
      if ($block->getIsActive()) {
        return $block;
      }
    }

    return null;
  }

  /**
   *
   * @return string
   */
  public function getTitle() {
    $block = $this->_getAvailableBlock();
    if ($block) {
      return $block->getTitle();
    }
  }

  /**
   *
   * @return string
   */
  public function getContent() {
    $html = '';

    $block = $this->_getAvailableBlock();
    if ($block) {
      $helper = Mage::helper('cms');
      $processor = $helper->getBlockTemplateProcessor();
      $html = $processor->filter($block->getContent());
    }

    return $html;
  }

  /**
   * Prepare Content HTML
   *
   * @return string
   */
  protected function _toHtml() {
    return $this->getContent();
  }
}