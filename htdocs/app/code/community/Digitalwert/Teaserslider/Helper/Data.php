<?php

class Digitalwert_Teaserslider_Helper_Data extends Mage_Core_Helper_Abstract
{

  /**
   *
   *
   * @param $url
   *
   * @return string
   */
  public function prepareUrl($url) {
    $urlParts = parse_url($url);

    if($urlParts == false) {
      return '';
    }

    if(!isset($urlParts['scheme']) && $url[0] != '/') {
      return '/' . $url;
    }

    if(isset($urlParts['scheme']) && $urlParts['scheme'] != 'http' && $urlParts['scheme'] != 'https') {
      return '';
    }

    return $url;
  }
}