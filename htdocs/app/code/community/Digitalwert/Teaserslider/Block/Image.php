<?php
/**
 * Widget content block
 *
 * @category   Digitalwert
 * @package    Digitalwert_Teaserslider
 * @author     Steve Rohrlack <steve.rohrlack@digitalwert.de>
 */
class Digitalwert_Teaserslider_Block_Image extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

  /**
   * Constructor
   */
  protected function _construct() {
    $this->setTemplate('teaserslider/image.phtml');

    parent::_construct();
  }

  /**
   *
   *
   * @return string
   */
  public function getImageUrl() {
    return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->getData('image');
  }

  /**
   *
   *
   * @return string
   */
  public function getLinkUrl() {
    return Mage::helper('teaserslider')->prepareUrl($this->getData('link_url'));
  }
}