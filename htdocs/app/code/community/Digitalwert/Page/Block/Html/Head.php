<?php

class Digitalwert_Page_Block_Html_Head extends Mage_Page_Block_Html_Head
{
    public function getPrefetchingLinks(){
        $resultLinks = '';
        $actionName = $this->getAction()->getFullActionName();
        $linkPrev = false;
        $linkNext = false;
        $preRenderUrl = false;
        $productCollection = false;

        if ($actionName == 'catalog_category_view') { // Category Page
            $layer = $this->getLayer();
            $productCollection = $layer->getProductCollection();
        } elseif($actionName == 'catalogsearch_result_index') { // Search result page
            $term = Mage::helper('catalogsearch')->getQueryText();
            $query = Mage::getModel('catalogsearch/query')->setQueryText($term)->prepare();
            Mage::getResourceModel('catalogsearch/fulltext')->prepareResult(
                Mage::getModel('catalogsearch/fulltext'),
                $term,
                $query
            );

            $collection = Mage::getResourceModel('catalog/product_collection');
            $collection->getSelect()->joinInner(
                array('search_result' => $collection->getTable('catalogsearch/result')),
                $collection->getConnection()->quoteInto(
                    'search_result.product_id=e.entity_id AND search_result.query_id=?',
                    $query->getId()
                ),
                array('relevance' => 'relevance')
            );
            $productCollection = $collection;
        }

        if($productCollection) {
            $tool = $this->getLayout()->createBlock('page/html_pager')
                ->setLimit($this->getLayout()->createBlock('catalog/product_list_toolbar')->getLimit())
                ->setCollection($productCollection);

            if ($tool && $tool->getCollection()->getSelectCountSql()){
                if ($tool->getLastPageNum() > 1){
                    if (!$tool->isFirstPage()){
                        $linkPrev = true;
                        if ($tool->getCurrentPage() == 2){
                            $url = explode('?', $tool->getPreviousPageUrl());
                            $prevUrl = @$url[0];
                        } else {
                            $prevUrl = $tool->getPreviousPageUrl();
                        }
                    }
                    if (!$tool->isLastPage()){
                        $linkNext = true;
                        $nextUrl = $tool->getNextPageUrl();
                    }
                }
            }
        }

        if($actionName == 'checkout_cart_index') { //cart page
            $preRenderUrl = '/checkout/onepage/';
        }

        if ($linkPrev){
            $resultLinks .= '<link rel="prev" href="' . $prevUrl . '" />';
            $resultLinks .= '<link rel="prefetch" href="'.$prevUrl.'" />';
        }
        if ($linkNext){
            $resultLinks .= '<link rel="next" href="'.$nextUrl.'" />';
            $resultLinks .= '<link rel="prefetch" href="' . $nextUrl . '" />';
        }

        if ($preRenderUrl) {
            $resultLinks .= '<link rel="prerender" href="' . $preRenderUrl . '" />';
        }

        return $resultLinks;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    public function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if ($layer) {
            return $layer;
        }
        return Mage::getSingleton('catalog/layer');
    }


    public function getProduct()
    {
        return Mage::registry('current_product');
    }

    public function getMetaTitle()
    {
        $product = $this->getProduct();
        if (!$product) {
            return parent::getMetaTitle();
        }

        $productMetaTitle = $product->getMetaTitle();

        if(!empty($productMetaTitle)){
            return $productMetaTitle;
        }
        $jrBrand = $product->getAttributeText('jr_brand');
        $productName = $product->getName();
        return $this->__('buy %s %s', $jrBrand, $productName);
    }

    public function getDescription()
    {
        $product = $this->getProduct();
        if(!$product){
            return parent::getDescription();
        }
        $productMetaDescription = $product->getMetaDescription();
        if(!empty($productMetaDescription)){
            return $productMetaDescription;
        }
        $jrBrand = $product->getAttributeText('jr_brand');
        $productName = $product->getName();
        return $this->__('buy %s %s from Juwelier Roller. &#10004; expert advice &#10004; Juwelier since 1886 &#10004; own workshop', $jrBrand, $productName);
    }

}
