# Extension Digitalwert_Productattributesasgroups

## Zweck

* veraendert die Darstellung von Produkt-Eigenschaften auf der Produkt-Detailseite
* gruppiert Produkt-Eigenschaften

## Implementierung

**gruppiert Produkt-Eigenschaften**

erweitert Block Mage_Catalog_Block_Product_View_Attributes

Die Gruppierung erfolgt anhand der Einteilung der Attribute im zum Produkt zugeordneten Attribut-Set.

## Dependencies

keine

## Konfiguration

keine