<?php
class Digitalwert_Productattributesasgroups_Block_Product_View_Attributes extends Mage_Catalog_Block_Product_View_Attributes
{
    /**
     * $excludeAttr is optional array of attribute codes to
     * exclude them from additional data array
     *
     * @param array $excludeAttr
     * @return array
     */
    public function getAdditionalData(array $excludeAttr = array())
    {
        $data = array();
        $product = $this->getProduct();
        $attributes = $product->getAttributes();
        foreach ($attributes as $attribute) {
            if ($attribute->getIsVisibleOnFront() && !in_array($attribute->getAttributeCode(), $excludeAttr)) {

                $attributeRawValue = $product->getData($attribute->getAttributeCode());
                if (is_null($attributeRawValue)) {
                    continue;
                }

                $value = $attribute->getFrontend()->getValue($product);

                if (!$product->hasData($attribute->getAttributeCode())) {
                    $value = Mage::helper('catalog')->__('N/A');
                } elseif ((string)$value == '') {
                    $value = Mage::helper('catalog')->__('No');
                } elseif ($attribute->getFrontendInput() == 'price' && is_string($value)) {
                    $value = Mage::app()->getStore()->convertPrice($value, true);
                }

                if (is_string($value) && strlen($value)) {
                    $groupId = 0;
                    if($currentAttributeGroupId = $attribute->getData('attribute_group_id')) {
                        $groupId = $currentAttributeGroupId;
                    }

                    $data[$groupId]['items'][ $attribute->getAttributeCode()] = array(
                      'label' => $attribute->getStoreLabel(),
                      'value' => $value,
                      'code'  => $attribute->getAttributeCode()
                    );

                    $data[$groupId]['attrid'] = $attribute->getId();
                }
            }
        }

        foreach($data as $groupId => $group) {
            $groupModel = Mage::getModel('eav/entity_attribute_group')->load($groupId);
            $data[$groupId]['title'] = $groupModel->getAttributeGroupName();
        }

        return $data;
    }
}
