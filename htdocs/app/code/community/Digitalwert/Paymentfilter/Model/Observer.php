<?php

/**
 * Catalog Observer Overwrite
 *
 * @category   Digitalwert
 * @package    Digitalwert_Paymentfilter
 * @author     Steve Rohrlack <steve.rohrlack@digitalwert.de>
 */
class Digitalwert_Paymentfilter_Model_Observer
{

  const PAYMENT_METHOD_PAYMORROW = 'paymorrow';
  const PAYMENT_METHOD_INVOICE = 'purchaseorder';

  /**
   *
   *
   * @param Varien_Event_Observer $observer
   *
   * @return $this
   */
  public function onPaymentMethodIsActive(Varien_Event_Observer $observer) {
    $event = $observer->getEvent();

    $method = $event->getMethodInstance();

    if ($method->getCode() == self::PAYMENT_METHOD_PAYMORROW || $method->getCode() == self::PAYMENT_METHOD_INVOICE) {
      $result = $event->getResult();

      $checkout = Mage::getSingleton('checkout/session')->getQuote();
      $billAddress = $checkout->getBillingAddress();

      $company = $billAddress->getCompany();

      if (!empty($company)) {
        //geschaeftskunde
        if ($method->getCode() == self::PAYMENT_METHOD_PAYMORROW) {
          $result->isAvailable = false;
        }

        if ($method->getCode() == self::PAYMENT_METHOD_INVOICE) {
          $result->isAvailable = true;
        }
      } else {
        //privatekunde
        if ($method->getCode() == self::PAYMENT_METHOD_PAYMORROW) {
          $result->isAvailable = true;
        }

        if ($method->getCode() == self::PAYMENT_METHOD_INVOICE) {
          $result->isAvailable = false;
        }
      }
    }

    return $this;
  }
}