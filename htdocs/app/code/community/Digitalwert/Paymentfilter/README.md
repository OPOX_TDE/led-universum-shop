# Extension Digitalwert_Paymentfilter

## Zweck

* dient zur Aktivierung/Deaktivierung von Zahlungsarten im Checkout fuer Privat- und Geschaeftskunden

## Implementierung

**Aktivierung/Deaktivierung von Zahlungsarten im Checkout fuer Privat- und Geschaeftskunden**

Es wird ein Observer fuer das Magento-Event "payment_method_is_active" verwendet.

siehe Model/Observer::onPaymentMethodIsActive

Als Privatkunde wird eine Nutzer eingestuft, wenn seine Rechnungsadresse im Checkout keinen Wert im Feld
"Firma" (company) enthaelt. Im Umkehrschluss wird ein Nutzer bei ausgefuelltem "Firma" Feld
als Geschaeftskunde eingestuft.

Die Zahlungsart "paymorrow" (Paymorrow Rechnungskauf) wird fuer Geschaeftskunden deaktiviert.

Die Zahlungsart "purchaseorder" (Abruf aus Auftrag - Rechnung) wird fuer Privatkunden deaktiviert.

## Dependencies

keine

## Konfiguration

keine