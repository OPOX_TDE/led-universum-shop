<?php

class Digitalwert_Imprint_Helper_Data extends Mage_Core_Helper_Abstract
{

  /**
   *
   *
   * @param string $rawPhoneNumber
   *
   * @return string
   */
  public function formatPhoneNumber($rawPhoneNumber) {
    return str_replace(array(' ', '-', '#', '/', '\\', '*'), '', $rawPhoneNumber);
  }
}