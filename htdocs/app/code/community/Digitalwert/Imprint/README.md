# Extension Digitalwert_Imprint

## Zweck

* Bereitstellung zusaetzlicher Konfigurationen fuer den aktiven Store
* Formatierung von Telefonnummern

## Implementierung

**Bereitstellung zusaetzlicher Konfigurationen fuer den aktiven Store**

siehe "Konfiguration"

Die Konfigurationsmoeglichkeiten werden in der system.xml dieser Extension definiert.
 
**Formatierung von Telefonnummern**

siehe Helper/Data

Die Methode "formatPhoneNumber" entfernt Telefonnummer-spezifische Sonderzeichen aus dem uebergebenen String.

## Dependencies

keine

## Konfiguration

* general/store_information/hours2 - zusaetzliche Store Oeffnungszeiten
* general/store_information/phone_support - Store Telefonnummer