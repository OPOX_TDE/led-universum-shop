<?php
class Digitalwert_Categoryattributes_Block_List_Footer
  extends Mage_Core_Block_Template
{

  const CACHE_LIFETIME = 86400;

  const CACHE_KEY = 'DIGITALWERT_CATEGORYATTRIBUTES_FOOTER';

  /**
   * Initialize block's cache
   */
  protected function _construct() {
    parent::_construct();

    $this->addData(array('cache_lifetime' => self::CACHE_LIFETIME));
    $this->addCacheTag(Mage_Catalog_Model_Category::CACHE_TAG);
  }

  /**
   * Get Key pieces for caching block content
   *
   * @return array
   */
  public function getCacheKeyInfo() {
    return array(
      self::CACHE_KEY,
      Mage::app()->getStore()->getId(),
      Mage::getDesign()->getPackageName(),
      Mage::getDesign()->getTheme('template'),
      'template' => $this->getTemplate(),
    );
  }

  /**
   *
   *
   * @return Mage_Catalog_Model_Resource_Category_Collection
   */
  public function getCategoryCollection() {
    $store = Mage::app()->getStore()->getId();

    $categoryCollection = Mage::getModel('catalog/category')
      ->getCollection()
      ->setStoreId($store)
      ->addAttributeToSelect('name')
      ->addAttributeToSelect('url_path')
      ->addAttributeToFilter('is_active', 1)
      ->addAttributeToFilter('show_in_footer', 1);

    return $categoryCollection;
  }
}