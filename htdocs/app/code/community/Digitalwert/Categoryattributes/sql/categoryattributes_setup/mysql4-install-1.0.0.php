<?php
$installer = $this;
$installer->startSetup();
$entityTypeId     = $installer->getEntityTypeId('catalog_category');
$attributeSetId   = $installer->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $installer->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

//show_in_footer
$installer->addAttribute('catalog_category', 'show_in_footer',  array(
  'type'         => 'int',
  'label'        => 'Im Footer zeigen',
  'input'        => 'select',
  'source'       => 'eav/entity_attribute_source_boolean',
  'global'       => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
  'visible'      => true,
  'required'     => false,
  'user_defined' => false,
  'default'      => 0,
));
$installer->addAttributeToGroup(
  $entityTypeId,
  $attributeSetId,
  $attributeGroupId,
  'show_in_footer',
  '11'
);

//description_bottom
$installer->addAttribute('catalog_category', 'description_bottom',  array(
  'type'                     => 'text',
  'label'                    => 'Beschreibung (unterhalb)',
  'input'                    => 'textarea',
  'wysiwyg_enabled'          => true,
  'visible_on_front'         => true,
  'is_html_allowed_on_front' => true,
  'global'                   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
  'visible'                  => true,
  'required'                 => false,
  'user_defined'             => false,
  'default'                  => '',
));
$installer->addAttributeToGroup(
  $entityTypeId,
  $attributeSetId,
  $attributeGroupId,
  'description_bottom',
  '12'
);