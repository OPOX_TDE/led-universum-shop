# Extension Digitalwert_Categoryattributes

## Zweck

* Verwaltung zusaetzlicher Kategorie-Attribute
* Bereitstellung eines eigenen Blocks fuer eine Liste von Kategorien, welche im Page Footer dargestellt werden sollen 

## Implementierung

**Verwaltung zusaetzlicher Kategorie-Attribute**

Es werden Magento "Install"-Skripte verwendet, um zusaetzliche Kategorie-Atrribute zu definieren.
 
**Bereitstellung eines eigenen Blocks fuer eine Liste von Kategorien**

siehe Block/List/Footer

In der Methode "getCategoryCollection" werden alle Kategorien ausgelesen welche aktiv sind (is_active) und im Footer
dargestellt werden sollen (show_in_footer).

## Dependencies

keine

## Konfiguration

keine