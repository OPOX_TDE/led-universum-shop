<?php

/**
 * Core data helper
 *
 */
class Digitalwert_Core_Helper_String extends Mage_Core_Helper_String
{
    public function createFilename($string) {
        return preg_replace("/[^A-Za-z0-9- ]/", '', str_replace(' ', '-', strtolower($string)));
    }

}
