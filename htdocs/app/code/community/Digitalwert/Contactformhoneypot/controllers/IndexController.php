<?php
require_once(Mage::getModuleDir('controllers', 'Mage_Contacts') . DS . 'IndexController.php');

/**
 * Class Digitalwert_Contactformhoneypot_IndexController
 *
 * @author Steve Rohrlack<steve.rohrlack@digitalwert.de>
 * @package Digitalwert_Contactformhoneypot
 * @copyright  Copyright (c) 2015 digitalwert GmbH (http://www.digitalwert.de)
 */
class Digitalwert_Contactformhoneypot_IndexController extends Mage_Contacts_IndexController
{

  public function postAction() {
    $post = $this->getRequest()->getPost();
    if ($post) {
      $translate = Mage::getSingleton('core/translate');
      /* @var $translate Mage_Core_Model_Translate */
      $translate->setTranslateInline(false);

      try {
        $error = false;

        /**
         * Honeypot field
         */
        $honeypotFieldName = Mage::getStoreConfig('contacts/honeypot/formfield_name');

        if (Zend_Validate::is(trim($post[$honeypotFieldName]), 'NotEmpty')) {
          $error = true;
        }

        if ($error) {
          throw new Exception();
        }

        unset($post[$honeypotFieldName]);

        return parent::postAction();
      } catch (Exception $e) {
        $translate->setTranslateInline(true);

        Mage::getSingleton('customer/session')->addError(Mage::helper('contacts')->__('Unable to submit your request. Please, try again later'));
        $this->_redirect('*/*/');
        return;
      }

    } else {
      $this->_redirect('*/*/');
    }
  }
}