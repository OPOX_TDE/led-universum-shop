<?php

class Digitalwert_Widgets_Block_Link extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

  /**
   * Constructor
   */
  protected function _construct() {
    $this->setTemplate('widgets/link.phtml');

    parent::_construct();
  }

    /**
     *
     *
     * @return string
     */
    public function getImageUrl() {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . $this->getData('image');
    }

}