<?php

class Digitalwert_Widgets_Block_Brandslider extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{

  /**
   * Constructor
   */
  protected function _construct() {
    $this->setTemplate('widgets/brand_slider.phtml');

    parent::_construct();
  }

  /**
   *
   *
   * @return collection | null
   */
  public function getAllBrands() {
    $brandsRootCategoryId = Mage::getStoreConfig(Digitalwert_Catalog_Helper_Category::CATEGORY_BRANDS_ID_XMLPATH);

    if (empty($brandsRootCategoryId)) {
      return null;
    }

    $brandsRootCategory = Mage::getModel('catalog/category')->load($brandsRootCategoryId);

    $brands = Mage::getModel('catalog/category')->getCollection()
        ->addFieldToSelect('entity_id')
        ->addFieldToSelect('name')
        ->addUrlRewriteToResult()
        ->addFieldToFilter('path', ['like' => $brandsRootCategory->getData('path') . '/%'])
        ->addLevelFilter($brandsRootCategory->getLevel() + 1);

    if ($brands->count() == 0) {
      return null;
    }

    return $brands;
  }

  /**
   *
   *
   * @param string $name
   *
   * @return string
   */
  public function getImageName($name) {
    return $this->helper('core/string')->createFilename($name) . '.png';
  }

}