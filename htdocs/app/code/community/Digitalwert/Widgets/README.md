# Extension Digitalwert_Widgets

## Zweck

* erweitert das bestehende Magento Widget "catalog_category_link"

## Implementierung

**erweitert das bestehende Magento Widget "catalog_category_link"**

Dem Widget wird eine zusaetzliche Eigenschaft "Image" (Bild) hinzugefuegt.

siehe etc/widget.xml

## Dependencies

* Mage_Catalog
* Mage_Widget

## Konfiguration

keine