<?php
/**
 * @see http://www.webguys.de/magento/magento-meldet-der-lagerbestand-des-artikels-ist-ungultig/
 */

include './../app/Mage.php';

// Set store defaults for Magento
$storeId = Mage::app()->getStore()->getId();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$pModel = Mage::getModel('catalog/product');

$products = $pModel->getCollection();

$maxItems = 20;
$i = 1;
foreach ($products as $product) {
  if ($i++ >= $maxItems) {
    break;
  }

  $stockData = $product->getStockData();

  if (!$stockData) {
    $product = $product->load($product->getId());
    $stockData = array(
        'manage_stock' => 0,
        'is_in_stock'  => 1,
        'qty'          => 1
    );
    $product->setStockData($stockData);

    echo $product->getSku() . "\n";
    ob_end_flush();

    try {
      $product->save();
    } catch (Exception $e) {
      echo $e->getMessage();
    }
  }
}