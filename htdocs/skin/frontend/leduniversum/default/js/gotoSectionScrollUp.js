/**
 * Created by romandorokhov on 03.03.16.
 */
Checkout.prototype.original_gotoSection = Checkout.prototype.gotoSection;

Checkout.prototype.gotoSection = function(section, reloadProgressBlock) {

    this.original_gotoSection(section, reloadProgressBlock);
    $$('.page')[0].scrollTo();
};