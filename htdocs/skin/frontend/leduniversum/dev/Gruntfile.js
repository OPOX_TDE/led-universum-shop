// module.exports = function (grunt) {
//     'use strict';
//     require('load-grunt-config')(grunt);
// };


module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-svgmin');
    grunt.loadNpmTasks('grunt-svgstore');

    grunt.registerTask('serve', ['uglify', 'sass', 'autoprefixer', 'svgmin', 'svgstore', 'watch']);

    grunt.initConfig({
        // copy: {
        //     project_css_js: {
        //         files: [
        //             {
        //                 "expand": true,
        //                 "cwd": "build/StyleSheets/",
        //                 "src": ["**"],
        //                 "dest": "../../htdocs/typo3conf/ext/ventum_s_landingpage/Resources/Public/Css/"
        //             },
        //             {
        //                 expand: true,
        //                 cwd: 'build/JavaScript/',
        //                 src: 'main.js',
        //                 dest: '../../htdocs/typo3conf/ext/ventum_s_landingpage/Resources/Public/JavaScript/',
        //                 filter: 'isFile'
        //             }
        //         ]
        //     }
        // },
        sass: {
            dist: {
                files: {
                    '../default/css/styles.css': '../default/scss/styles.scss'
                },
                options: {
                    style: 'compressed'
                }
            }
        },
        autoprefixer: {
            options: {
                browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']
                //diff: true
            },
            single_file: {
                src: '../default/css/style.css'
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            my_target: {
                files: {
                    '../default/js/app-extended.js': [
                        // 'src/JavaScript/main/*.js',
                        '../default/js/main.js'
                    ]
                    // '../../htdocs/typo3conf/ext/swm_stadtwerke_template/Resources/Public/JavaScript/plugins.js': ['media/JavaScript/plugins.js'],
                    // '../../htdocs/typo3conf/ext/swm_stadtwerke_template/Resources/Public/JavaScript/form-personal-data.js': ['media/JavaScript/form-personal-data.js'],
                    // '../../htdocs/typo3conf/ext/swm_stadtwerke_template/Resources/Public/JavaScript/jquery.spin.js': ['media/JavaScript/spin.js', 'media/JavaScript/jquery.spin.js'],
                    // '../../htdocs/typo3conf/ext/swm_stadtwerke_template/Resources/Public/JavaScript/stacktable.js': ['media/JavaScript/stacktable.js']
                }
            }
        },
        svgmin: {
            options: {
                "plugins": [
                    {
                        "removeViewBox": false
                    },
                    {
                        "removeUselessStrokeAndFill": false
                    },
                    {
                        "removeEmptyAttrs": false
                    },
                    {
                        "removeTitle": true
                    }
                ]
            },
            mono_sprite: {
                options: {
                    "plugins": [{
                        "removeAttrs": {
                            "attrs": ["fill"]
                        }
                    }]
                },
                "files": [{
                    "expand": true,
                    "src": ["svgs/sprite/*.svg"]
                }]
            },
            color_sprite: {
                "files": [{
                    "expand": true,
                    "src": ["svgs/sprite/color/*.svg"]
                }]
            }
        },
        svgstore: {
            options: {
                "prefix": "icon-"
            },
            icons: {
                "files": {
                    "icons/sprite.svg": ["svgs/sprite/**/*.svg"]
                }
            }
        },
        watch: {
            scss: {
                files: ['../default/scss/**/*.scss'],
                tasks: ['sass', 'autoprefixer']
            },

            svg: {
                files: ['svgs/sprite/*.svg'],
                tasks: ['svgmin', 'svgstore']
            },
            // html: {
            //     files: ['build/*.html'],
            //     options: {
            //         livereload: true
            //     }
            // },
            js: {
                files: ['../default/js/main.js'],
                // tasks: ['uglify', 'copy:project_css_js'],
                tasks: ['uglify'],
                options: {
                    livereload: true
                }
            },
            css: {
                files: ['../default/css/style.css'],
                options: {
                    livereload: true
                }
            }
        },
        connect: {
            server: {
                options: {
                    port: 8000,
                    hostname: '*',
                    keepalive: true,
                    base: '../'
                }
            }
        }
    });
};

